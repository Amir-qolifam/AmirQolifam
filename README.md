
## Hi there<img src="assets/images/emoji/hi.webp" width="25px"> I am Amir Qolifam



### Reach me on <img src="assets/images/emoji/Magnifying-Glass-Tilted-Right.gif" width="25px">:

[![AmirQolifam | AmirQolifam | ProtonMail](https://img.shields.io/badge/ProtonMail-8B89CC?style=for-the-badge&logo=protonmail&logoColor=white)](mailto:amir.qolifam.d@protonmail.com)
[![AmirQolifam | AmirQolifam | Telegram](https://img.shields.io/badge/Telegram-2CA5E0?style=for-the-badge&logo=telegram&logoColor=white)](https://t.me/Amir_qo)
[![AmirQolifam | AmirQolifam | Instagram](https://img.shields.io/badge/Instagram-E4405F?style=for-the-badge&logo=instagram&logoColor=white)](https://www.instagram.com/amir_qolifam/)
[![AmirQolifam | AmirQolifam | Stack_Overflow](https://img.shields.io/badge/Stack_Overflow-FE7A16?style=for-the-badge&logo=stack-overflow&logoColor=white)](https://stackoverflow.com/users/16805264/amir)
[![AmirQolifam | AmirQolifam | Stack_Overflow](https://img.shields.io/badge/GitHub-100000?style=for-the-badge&logo=github&logoColor=white)](https://github.com/AmirQolifam)

---
 <img align="right" alt="GIF" src="assets/images/code.gif" width="520" height="290" />

### <img src="assets/images/emoji/smiling-face-with-sunglasses-1.gif" width="30px">  More About Me:
- <img src="assets/images/emoji/face-with-monocle.gif" width="25px"> Im Student!!!
- <img src="assets/images/emoji/Dart_WIN-1.gif" width="25px"> I’m currently working on ... Developing of my technical skills
- <img src="assets/images/emoji/fire-1.gif" width="25px">  I’m currently learning ... Css
- <img src="assets/images/emoji/Handshake.gif" width="25px"> I’m looking to collaborate on ... Best Developers :)
- <img src="assets/images/emoji/memo-2.gif" width="25px"> Ask me about ... Anything
- <img src="assets/images/emoji/High-Voltage.gif" width="25px"> I love wath movie!

---

### 🔨 Languages :

[![Python](https://img.shields.io/badge/Python-FFD43B?style=for-the-badge&logo=python&logoColor=darkgreen)](https://www.python.org/)
[![HTML5](https://img.shields.io/badge/HTML5-E34F26?style=for-the-badge&logo=html5&logoColor=white)](https://html.com/html5/)
[![CSS3](https://img.shields.io/badge/CSS3-1572B6?style=for-the-badge&logo=css3&logoColor=white)](https://css-tricks.com/)

### ⚙️ Tools :

[![Linux](https://img.shields.io/badge/Linux-FCC624?style=for-the-badge&logo=linux&logoColor=black)](https://www.linux.org/)
[![PyCharm](https://img.shields.io/badge/pycharm-143?style=for-the-badge&logo=pycharm&logoColor=black&color=black&labelColor=green)](https://www.jetbrains.com/)
[![WebStorm](https://img.shields.io/badge/webstorm-143?style=for-the-badge&logo=webstorm&logoColor=black&color=black&labelColor=green)](https://www.jetbrains.com/)
[![Git](https://img.shields.io/badge/Git-F05032?style=for-the-badge&logo=git&logoColor=white)](https://git-scm.com/)
[![Canva](https://img.shields.io/badge/Canva-%2300C4CC.svg?style=for-the-badge&logo=Canva&logoColor=white)](https://www.canva.com/)
[![Visual Studio Code](https://img.shields.io/badge/Visual%20Studio%20Code-0078d7.svg?style=for-the-badge&logo=visual-studio-code&logoColor=white)](https://code.visualstudio.com/)
[![Notion](https://img.shields.io/badge/Notion-%23000000.svg?style=for-the-badge&logo=notion&logoColor=white)](https://notion.so)
[![oh my zh](https://img.shields.io/badge/oh_my_zsh-1A2C34?style=for-the-badge&logo=ohmyzsh&logoColor=white)](https://ohmyz.sh/)

### 👨🏻‍💻 Workspace
[![Laptop](https://img.shields.io/badge/lenovo-laptop-E2231A?style=for-the-badge&logo=lenovo&logoColor=white)](https://www.lenovo.com/)
[![CPU](https://img.shields.io/badge/Intel-Core_i3_-0071C5?style=for-the-badge&logo=intel&logoColor=white)](https://www.lenovo.com/)
[![RAM memory](https://img.shields.io/badge/RAM-4GB-%230071C5.svg?&style=for-the-badge&logoColor=white)](https://www.lenovo.com/)
### <img src="assets/images/emoji/grinning-face-with-smiling-eyes.gif" width="35px"> Hobby
[![Netflix](https://img.shields.io/badge/Netflix-E50914?style=for-the-badge&logo=netflix&logoColor=white)](https://www.netflix.com/)
[![Sound Cloud](https://img.shields.io/badge/sound%20cloud-FF5500?style=for-the-badge&logo=soundcloud&logoColor=white)](https://soundcloud.com)
[![YouTube Music](https://img.shields.io/badge/YouTube_Music-FF0000?style=for-the-badge&logo=youtube-music&logoColor=white)](https://music.youtube.com/)


### <img src="assets/images/logo/business-graph.gif" width="35px">  My GitHub Stats
<a href="https://github.com/AmirQolifam">
<img align="center" src="https://github-readme-stats.vercel.app/api?username=AmirQolifam&show_icons=true&count_private=false&include_all_commits=true" /></a>

<a href="https://github.com/AmirQolifam">
<img align="center" src="https://github-readme-stats.vercel.app/api/top-langs/?username=AmirQolifam" />
</a>

<br>

---

<img src="https://i.ibb.co/0MZzJ2d/download.png" border="0">
